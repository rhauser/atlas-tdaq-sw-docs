# ATLAS Trigger/DAQ Software Documentation

This repository contains the source of the ATLAS TDAQ software documentation.

## Generate the documentation locally

Checkout this repository and run `antora`:

```shell
git clone https://gitlab.cern.ch/atlas-tdaq-software/docs/atlas-tdaq-sw-docs.git
cd atlas-tdaq-sw-docs
podman run -it -v $(pwd):/antora gitlab-registry.cern.ch/atlas-tdaq-software/docs/atlas-tdaq-sw-docs/antora:latest  generate antora-playbook.yml
```
Point your browser to `file:///$(pwd)/build/site/atlas-tdaq-sw/latest/index.html`.

## Add to the documentation

Fork this repository at https://gitlab.cern.ch/atlas-tdaq-software/docs/atlas-tdaq-sw-docs

Then checkout your fork

```shell
git clone https://gitlab.cern.ch/rhauser/atlas-tdaq-software/docs/atlas-tdaq-sw-docs.git
git remote add upstream  https://gitlab.cern.ch/atlas-tdaq-software/docs/atlas-tdaq-sw-docs.git
```

To start development create a new feature branch:
```shell
cd atlas-tdaq-sw-docs
git fetch upstream
git checkout -b my-new-feature upstream/main
```

Make your changes, optionally check them with the command above, then push to your
repository:

```shell
git push -u origin my-new-feature
```

Go to your repository and create a merge request to the original repository. You can
do this also directly on the command line, specifying the target branch is optional,
the default is `main`.

```shell
git push -o merge_request.create -o merge_request.target=main origin my-new-feature
```

Your merge request will be automatically processed and you will be able to see
the result at https://atlas-tdaq-sw.web.cern.ch/review/my-new-feature/atlas-tdaq-sw/latest

After review your branch will be merged and the results will be at
https://atlas-tdaq-sw.web.cern.ch/docs/atlas-tdaq-sw/latest
