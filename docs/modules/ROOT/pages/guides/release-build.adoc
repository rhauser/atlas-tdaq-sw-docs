= Building a TDAQ Release
:source-language: shell

These instruction describe the official release build
procedure.

TIP: See xref:#manual-build[below] for manual build instructions for experts.

== Prerequisites

All releases are typically branched from the nightly branch
(tdaq-99-00-00). All code should be part of the nightly well before the
release date, so it can be tested from the nightly cvmfs installation.

NOTE: From tdaq-09-05-00 on forward the tdaq-common release always has the
same version number as the TDAQ release itself.

In the following we use `tdaq-12-00-00` as an example.

You need the necessary permissions to create a new branch
in the `tdaq-cmake` and `tdaq-common-cmake` repositories.

== Branch the release

----
mkdir new-release
cd new-release
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/cmake/scripts/prepare_release 12.0.0 [ LCG_release ]
----

Specifying the LCG release should not be necessary as the version number
should be already part of the tdaq-common `CMakeLists.txt` file.

Note that the version is specified as a CMake version, not in the TDAQ
specific `tdaq-12-00-00` style.

The script will check out both tdaq-common and tdaq projects, branch
them and modify the top level `CMakeLists.txt` files as needed. Normally
there is nothing else to do. The script will print instructions on what
to do, something like:

----
(cd tdaq-common-cmake && git commit -a && git push origin tdaq-common-12-00-00)
(cd tdaq-cmake && git commit -a && git push origin tdaq-12-00-00)
----

[TIP]
.Possible adjustments before the commit are

* Change the configurations that will be built (including the patches
jobs), e.g. if this a special release which does not need a certain build
configuration.

== Build the release

* Go the https://gitlab.cern.ch/atlas-tdaq-software/tdaq-cmake/-/pipeline_schedules[pipeline schedules]
page and create a new pipeline schedule for the release branch (here: `tdaq-12-00-00`).

* Create a variable `OPTIONS` and set its value to `--rpm`
* Set the pipeline to `inactive` (we trigger it manually) and save it
* Run the pipeline by clicking the button

You should get an e-mail notification when the pipeline is (hopefully
successfully) finished. At this point the new RPMs are in the these
locations:

----
/eos/user/a/atdsoft/www/nightlies/tdaq-common-12-00-00/<configuration>/
/eos/user/a/atdsoft/www/nightlies/tdaq-12-00-00/<configuration>/
----

Note that each directory has all the `arch` and `src` RPMs as well the
binary specific RPMs. In the final YUM repository structure we will keep
only one of these. It should be the one from the official configuration, 
e.g. `x86_64-el9-gcc13-opt` as the others may differ in subtle ways.

Go to the xref::guides/yum-repo.adoc[yum repo installation] instructions
for the next steps.

[#manual-build]
== Manual Release Building

An expert might want to build a TDAQ release locally for easier
testing or developing a new feature that might break many packages
across the board.

A full release build requires a lot of resources. It should be done
on a machine with many cores (e.g. a `pc-tbed-pub` host), and at least
15 GByte of available scratch space.

For repeated build `ccache` should be enabled. The cache directory
should be on a local scratch disk, not in your AFS home directory (the default
is `$HOME/.ccache).

=== Official build scripts

The preferred way is to use the official build scripts of the TDAQ release.

----
mkdir -p /scratch/rhauser/full
cd /scratch/rhauser/full
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_stack tdaq-common-99-00-00 tdaq-99-00-00 x86_64-el9-gcc13-opt
----

If you want to use `ccache`:

----
export CCACHE_DIR=/scratch/rhauser/ccache
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_stack tdaq-common-99-00-00 tdaq-99-00-00 x86_64-el9-gcc13-opt -D USE_CCACHE=TRUE
----

This will build the nightly release in the current working directory. The result will look like this:

----
/scratch/rhauser/full/
   tdaq-common/tdaq-common-99-00-00/            - source
   tdaq-common/tdaq-common-99-00-00/installed   - install dir
   tdaq/tdaq/                                   - source
   tdaq/tdaq-99-00-00/installed                 - install dir
   build/tdaq-common/tdaq-common-99-00-00/x86_64-el9-gcc13-opt/  - build dir
   build/tdaq/tdaq-99-00-00/x86_64-el9-gcc13-opt/  - build dir
----

The structure is such that you can do multiple builds from the same top level directory. E.g.

----
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_stack tdaq-common-99-00-00 tdaq-99-00-00 x86_64-el9-gcc13-dbg
----

will build the debug version and install it in parallel without disturbing the optimized build.

Note that cmake captures all output and errors and shows it on the https://atlas-tdaq-cdash.web.cern.ch/index.php?project=tdaq[Dashbord].

==== Single project builds

If you know for sure that you need only `tdaq-common` or only `tdaq`, you can just build a single project:

----
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_release tdaq-common tdaq-common-99-00-00 x86_64-el9-gcc13-dbg
----

or

----
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_release tdaq tdaq-99-00-00 x86_64-el9-gcc13-dbg
----

==== Manual checkout

If you just want to checkout a project without building it:

----
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/checkout_release tdaq-common tdaq-common-99-00-00
----

=== Interactive manual build

If you want to build the checked out release by hand:

----
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/setup.sh x86_64-el9-gcc13-dbg
mkdir build
cd build
cmake -D BINARY_TAG=x86_64-el9-gcc13-dbg ../tdaq-common/tdaq-common-99-00-00
make -j $(nproc) -k
make install/fast
cd ..
export TDAQ_RELEASE_BASE=$(pwd)
cm_setup tdaq-common-99-00-00
echo $TDAQC_INST_PATH
----

This is useful if you want to see the errors in your terminal rather than the dashboard, e.g. because
you are iteratively fixing many problems in different packages.

=== Options

The `build_release` and `build_stack` tools take a number of CMake and CTest options.
See the xref::guides/cmake.adoc[CMake guide] for more details.

