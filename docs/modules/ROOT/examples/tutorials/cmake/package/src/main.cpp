
#include "package/MyClass.h"

#include <iostream>
#include <cstdlib>

int main(int argc, char *argv[])
{
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << "NUMBER" << std::endl;
        exit(1);
    }
    daq::MyPackage::MyClass c(std::strtol(argv[1]));
    std::cout << "Value is " << c.get() << std::endl;
}
