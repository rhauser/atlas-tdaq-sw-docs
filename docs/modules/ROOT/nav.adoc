* xref::overview.adoc[Overview]
* Tutorials
** xref::tutorials/package/index.adoc[Developing a TDAQ package]
** Generating Partitions
** Running Partitions
* Guides
** xref::guides/cmake.adoc[Developer's Guide]
** xref::guides/release-build.adoc[Release Building]
** Installation and Deployment
*** xref::guides/yum-repo.adoc[Yum repo preparation]
*** xref::guides/deploy-testbed.adoc[Testbed]
*** xref::guides/deploy-p1.adoc[Point 1]
*** xref::guides/external-install.adoc[External]
** xref::guides/containers.adoc[Containers]
* Components
** Core
*** Inter Process Communication
*** Error Reporting System
** Configuration
*** Overview
*** `config` Layer
*** OKS
*** RDB
*** Partition Maker
** Monitoring
*** Information Service
*** Online Histogramming Service
**** Archiving Histograms
**** Gathering Histograms
*** Data Quality Framework
*** coca
** Run Control
** Readout
** Data flow
*** Phase 1
**** HLT Supervisor
**** Event Builder
**** HLT Processing Unit
**** Event Storage
**** Dataflow Protocols/Libraries
*** Phase 2
** RCD
** Web Services
*** Run Control
*** Information Service
*** Log files
* Training
* xref::documentation/index.adoc[Documentation]
